from Arena import Arena
import numpy as np
import matplotlib.pyplot as plt

# Create the population and run the tournament
######################################################################
A = Arena('Player_population/')
data = A.Tournament()


# Save the data
######################################################################
names = A.player_names
with open('tournament_results.txt', 'w') as f:
    f.write(str(names) +'\n')
    f.write(str(data))
