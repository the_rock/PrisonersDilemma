from glob import glob
import numpy as np
import importlib

class Arena(object):
    """
    Arena object holds the information about every player and during a tournament
    will select which ones face off. All properties of the match can be altered
    before-hand to customize the match. For example, changing the score matrix
    will alter the cost/benefit trade off of being mean or nice.

    For the player functions a specific format is required (down to where the
    spaces are):
    def Action(player, other):
        <your code>

    where <your code> returns either True or False. Where True means to share,
    and False means to take.
    """

    def __init__(self, population):
        """
        Initialize the arena by collecting information about all of the players.
        After extracting their names, create a single file with all of the
        player functions. This file is then accessed to get a function for each
        player that will be used to determine their behavior in each match.

        Also initialize the match parameters, such as the style of tournament
        and score matrix.
        """
        # Record the directory where player code is stored
        self.population = population
        # Record the path to each player defined function
        self.players = glob(population + '*.py')
        # Extract the name for each player
        self.player_names = list(p[p.rfind('/')+1:-3] for p in self.players)
        # Create a list of functions as defined by the users
        self.player_actions = []
        for i in range(len(self.players)):
            self.player_actions.append(importlib.import_module('%s.%s' % (self.population[:-1], self.player_names[i])).Action)

        # tournament style, random also implimented
        self.style = 'round-robin'
        # Score matrix
        #               take   share
        self.scores = [[[1,1], [5,0]], # take
                       [[0,5], [3,3]]] # share
        # Max/min number of rounds per match (random to prevent exploiting final game)
        self.rounds = {'min':195, 'max':205}
        # Number of rounds for round-robin, number of matches for random
        self.tournament_itterations = 1

    def Match(self, player_1, player_2):
        """
        Run a match between two players and record their behaviour at all
        steps. For a random number of rounds, each player's Action
        function is called, given only the historical data of their
        interactions.
        """

        # Dictionary to store all relevant data for the match
        results = {'n_rounds':np.random.randint(self.rounds['min'], self.rounds['max']+1),
                   'player_1':player_1,
                   'player_2':player_2,
                   'player_1_actions':[],
                   'player_2_actions':[],
                   'player_1_scores':[],
                   'player_2_scores':[],
                   'NULL game': False}
        round_multiplier = np.ones(results['n_rounds'])
        round_multiplier[np.random.randint(10, results['n_rounds'], 10)] = 3.
        round_multiplier[np.random.randint(10, results['n_rounds'], 2)] = 5.
        results['multiplier'] = round_multiplier

        for i in range(results['n_rounds']):
            # Compute each player's action
            try:
                player_1_action = self.player_actions[player_1](results['player_1_actions'], results['player_2_actions'], results['multiplier'][i])
                # Check the player returned a valid answer
                if player_1_action not in [True, False]:
                    raise Exception('Player 1 action is not True or False')
                
                player_2_action = self.player_actions[player_2](results['player_2_actions'], results['player_1_actions'], results['multiplier'][i])
                # Check the player returned a valid answer
                if player_2_action not in [True, False]:
                    raise Exception('Player 2 action is not True or False')
            except Exception as e:
                # If one player crashes, the game is marked as NULL and the match is terminated
                results['NULL game'] = True
                results['error_message'] = e
                break

            # Add the results of the round to the dictionary
            results['player_1_actions'].append(player_1_action)
            results['player_2_actions'].append(player_2_action)
            results['player_1_scores'].append(self.scores[player_1_action][player_2_action][0] * results['multiplier'][i])
            results['player_2_scores'].append(self.scores[player_1_action][player_2_action][1] * results['multiplier'][i])

        return results
        
    def Tournament(self):
        """
        This is used to run many matches between the players. The way that
        the players are selected for a match is determined by the
        style variable.
        """

        # Run whichever style of tournament is selected
        if self.style == 'round-robin':
            return self.Round_Robin()
        elif self.style == 'random':
            return self.Random_Match()

    def Round_Robin(self):
        """
        Every player is matched against every other player. The number
        of times that they face off is determined by the variable:
        tournament_itterations
        """

        data = []
        
        for i in range(self.tournament_itterations):
            # Pit each player against every other player
            for p1 in range(len(self.players)):
                for p2 in range(p1+1, len(self.players)):
                    data.append(self.Match(p1,p2))

        return data

    def Random_Match(self):
        """
        Players are randomly selected to match against each other.
        The number of matches that are run is determined by the
        variable: tournament_itterations
        """

        data = []

        for i in range(self.tournament_itterations):
            # Select two players randomly
            p1 = np.random.randint(0, len(self.players))
            p2 = p1
            # Ensure they don't face against themselves
            while p2 == p1:
                p2 = np.random.randint(0, len(self.players))

            data.append(self.Match(p1,p2))

        return data
                
