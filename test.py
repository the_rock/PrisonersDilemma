from Arena import Arena
import numpy as np
import matplotlib.pyplot as plt

# Create the population and run the tournament
# This actually does all the work
######################################################################
A = Arena('Player_population/')
data = A.Tournament()
names = A.player_names
######################################################################

# Collect the data for each player
player_scores = {}
for match in data:
    for i in [1,2]:
        if match['player_%i' % i] not in player_scores:
            player_scores[match['player_%i' % i]] = []

        player_scores[match['player_%i' % i]] += match['player_%i_scores' %i]

# Calculate the mean score for each player
X = []
names = []
Y = []
stds = []
bootstrap_std = []
for p in player_scores:
    X.append(p)
    names.append(A.player_names[p])
    Y.append(np.mean(player_scores[p]))
    stds.append(np.std(player_scores[p]) / np.sqrt(len(player_scores[p])))

# Plot in descending order
N = np.argsort(-np.array(Y))

plt.bar(range(len(Y)), np.array(Y)[N], yerr = np.array(stds)[N], width = 0.9, linewidth = 0, align = 'center')
plt.xticks(range(len(Y)), np.array(names)[N])
plt.ylabel('Mean Score Per Match')
plt.title('Player Rankings, with standard deviation / sqrt(N) uncertainties')
plt.show()
