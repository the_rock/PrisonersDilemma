# Forgiving Grudger

def Action(player, other_1, other_2):

    if sum(other_1) + sum(other_2) - len(other_1) - len(other_2) < -1:
        return False
    else:
        return True
