from glob import glob
import numpy as np

class Arena(object):
    """
    Arena object holds the information about every player and during a tournament
    will select which ones face off. All properties of the match can be altered
    before-hand to customize the match. For example, changing the score matrix
    will alter the cost/benefit trade off of being mean or nice.

    For the player functions a specific format is required (down to where the
    spaces are):
    def Action(player, other_1, other_2):
        <your code>

    where <your code> returns either True or False. Where True means to share,
    and False means to take.
    """

    def __init__(self, population):
        """
        Initialize the arena by collecting information about all of the players.
        After extracting their names, create a single file with all of the
        player functions. This file is then accessed to get a function for each
        player that will be used to determine their behavior in each match.

        Also initialize the match parameters, such as the style of tournament
        and score matrix.
        """
        # Record the directory where player code is stored
        self.population = population
        # Record the path to each player defined function
        self.players = glob(population + '*.py')
        # Extract the name for each player
        self.player_names = list(p[p.rfind('/')+1:-3] for p in self.players)
        # Create the temporary file with all the player defined functions
        self._Generate_TempPopulation()
        # Import the temporary file
        import TempPopulation
        # Create a list of functions as defined by the users
        self.player_actions = []
        for i in range(len(self.players)):
            self.player_actions.append(getattr(TempPopulation, 'Action_%i' % i))

        # tournament style, random also implimented
        self.style = 'round-robin'
        # Score matrix
        # Group Choices: FFF     TFF     TTF     TTT    
        self.scores  = [[1, 1], [5, 0], [9, 3], [7, 7]]
        # Your Choice:   F  T    F  T    F  T    F  T

        # Max/min number of rounds per match (random to prevent exploiting final game)
        self.rounds = {'min':100, 'max':105}
        # Number of rounds for round-robin, number of matches for random
        self.tournament_itterations = 1

    def _Generate_TempPopulation(self):
        """
        Combine all of the player functions into a single file to make it easier
        to access/import. Adjust the name of each function to guarantee a unique
        identifier when all the functions are in the same file. The unique
        identifier is just the player id number (determined in the init
        function).
        """

        temp_population = []
        i = 0
        while i < len(self.player_names):
            print 'on player %s' % (self.player_names[i])
            # Get the code written by the player
            with open(self.players[i],'r') as f:
                player_func = f.readlines()

            # Replace the "Action" function with a unique name
            found_func = False
            for l in range(len(player_func)):
                if player_func[l].strip() == 'def Action(player, other_1, other_2):':
                    player_func[l] = 'def Action_%i(player, other_1, other_2):' % i
                    found_func = True
                    break

            # Add this players function to the group
            if not found_func:
                print 'Unable to add player %s, could not find Action function' % self.player_names[i]
                self.player_names.pop(i)
                self.players.pop(i)
                continue
            temp_population += player_func
            i += 1

        with open('TempPopulation.py', 'w') as f:
            f.writelines(temp_population)

    def Match(self, player_1, player_2, player_3):
        """
        Run a match between two players and record their behaviour at all
        steps. For a random number of rounds, each player's Action
        function is called, given only the historical data of their
        interactions.
        """

        # Dictionary to store all relevant data for the match
        results = {'n_rounds':np.random.randint(self.rounds['min'], self.rounds['max']+1),
                   'player_1':player_1,
                   'player_2':player_2,
                   'player_3':player_3,
                   'player_1_actions':[],
                   'player_2_actions':[],
                   'player_3_actions':[],
                   'player_1_scores':[],
                   'player_2_scores':[],
                   'player_3_scores':[],
                   'NULL game': False}

        for i in range(results['n_rounds']):
            # Compute each player's action
            try:
                player_1_action = self.player_actions[player_1](results['player_1_actions'], results['player_2_actions'], results['player_3_actions'])
                # Check the player returned a valid answer
                if player_1_action not in [True, False]:
                    raise Exception('Player 1 action is not True or False')
                
                player_2_action = self.player_actions[player_2](results['player_2_actions'], results['player_1_actions'], results['player_3_actions'])
                # Check the player returned a valid answer
                if player_2_action not in [True, False]:
                    raise Exception('Player 2 action is not True or False')

                player_3_action = self.player_actions[player_3](results['player_3_actions'], results['player_2_actions'], results['player_1_actions'])
                # Check the player returned a valid answer
                if player_3_action not in [True, False]:
                    raise Exception('Player 3 action is not True or False')
            except Exception as e:
                # If one player crashes, the game is marked as NULL and the match is terminated
                results['NULL game'] = True
                results['error_message'] = e
                break

            # Add the results of the round to the dictionary
            results['player_1_actions'].append(player_1_action)
            results['player_2_actions'].append(player_2_action)
            results['player_3_actions'].append(player_3_action)
            results['player_1_scores'].append(self.scores[int(player_1_action) + int(player_2_action) +int(player_3_action)][int(player_1_action)])
            results['player_2_scores'].append(self.scores[int(player_1_action) + int(player_2_action) +int(player_3_action)][int(player_2_action)])
            results['player_3_scores'].append(self.scores[int(player_1_action) + int(player_2_action) +int(player_3_action)][int(player_3_action)])

        return results
        
    def Tournament(self):
        """
        This is used to run many matches between the players. The way that
        the players are selected for a match is determined by the
        style variable.
        """

        # Run whichever style of tournament is selected
        if self.style == 'round-robin':
            return self.Round_Robin()
        elif self.style == 'random':
            return self.Random_Match()

    def Round_Robin(self):
        """
        Every player is matched against every other player. The number
        of times that they face off is determined by the variable:
        tournament_itterations
        """

        data = []
        
        for i in range(self.tournament_itterations):
            # Pit each player against every other player
            # Player 1 ranges through all but the last two
            for p1 in range(0, len(self.players) - 2):
                # Player 2 ranges through everything after player 1, except the last option
                for p2 in range(p1+1, len(self.players) - 1):
                    # Player 3 ranges through what is left
                    for p3 in range(p2+1, len(self.players)):
                        data.append(self.Match(p1,p2,p3))

        return data

    def Random_Match(self):
        """
        Players are randomly selected to match against each other.
        The number of matches that are run is determined by the
        variable: tournament_itterations
        """

        data = []

        for i in range(self.tournament_itterations):
            # Select two players randomly
            p1 = np.random.randint(0, len(self.players))
            p2 = p1
            # Ensure they don't face against themselves
            while p2 == p1:
                p2 = np.random.randint(0, len(self.players))
            # Ensure the last player is not one of the already chosen ones
            p3 = p2
            while p3 in [p1, p2]:
                p3 = np.random.randint(0, len(self.players))
                
            data.append(self.Match(p1,p2,p3))

        return data
                
